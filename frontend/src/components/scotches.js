import React from "react";
import { createMuiTheme } from "@material-ui/core/styles";
import MaterialTable from "material-table";

let direction = "ltr";

// eslint-disable-next-line
const theme = createMuiTheme({
  direction: direction,
  palette: {
    type: "light"
  }
});

const Scotches = ({ scotches }) => {
  return (
    <div style={{ maxWidth: "100%" }}>
      <MaterialTable
        options={{
          pageSize: 50,
          pagingSize: 50
        }}
        columns={[
          { title: "ID", field: "uid" },
          { title: "Name", field: "name" },
          { title: "Type", field: "type" },
          { title: "Volume (ml)", field: "volume", type: "numeric" }
        ]}
        data={scotches}
        title="Scotches"
      />
    </div>
  );
};

export default Scotches;

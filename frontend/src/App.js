import React, { Component } from "react";
import Scotches from "./components/scotches";

class App extends Component {
  render() {
    return <Scotches scotches={this.state.scotches} />;
  }

  state = {
    scotches: []
  };

  componentDidMount() {
    fetch("http://0.0.0.0:5002/scotch")
      .then(res => res.json())
      .then(data => {
        this.setState({ scotches: data });
      })
      .catch(console.log);
  }
}

export default App;

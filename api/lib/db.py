import pymysql


class DBHelper:

    def __init__(self, host, user, password, db):
        self.host = host
        self.user = user
        self.password = password
        self.db = db

    def __connect__(self):
        self.con = pymysql.connect(host=self.host, user=self.user, password=self.password, db=self.db,
                                   cursorclass=pymysql.cursors.DictCursor, autocommit=True)
        self.cur = self.con.cursor()

    def __disconnect__(self):
        self.con.close()

    def fetch(self, sql):
        self.__connect__()
        self.cur.execute(sql)
        result = self.cur.fetchall()
        self.__disconnect__()
        return result

    def execute(self, sql):
        self.__connect__()
        self.cur.execute(sql)
        self.__disconnect__()

    def insert_scotch(self, scotch_number, scotch_name, scotch_url, scotch_image, scotch_type, scotch_volume,
                      formatted_date, scotch_price):
        """
        Performs an insert on our database to add entries for a single scotch.

        :param scotch_number: SAQ ID
        :param scotch_name: Name of scotch
        :param scotch_url: URL of scotch on SAQ.com
        :param scotch_image: URL of image
        :param scotch_type: Type of scotch
        :param scotch_volume: volume of scotch in mls
        :param formatted_date: Timestamp for the prices table for historical tracking
        :param scotch_price: Price of scotch
        :return: None. Values are written to the database
        """
        self.execute(
            f'INSERT INTO scotch(product_number, scotch_name, product_url, image_url, scotch_type, volume_value) \
            VALUES ({scotch_number}, "{scotch_name}","{scotch_url}", "{scotch_image}", "{scotch_type}", \
            "{scotch_volume}") ON DUPLICATE KEY UPDATE scotch_name="{scotch_name}", product_url="{scotch_url}", \
            image_url="{scotch_image}", scotch_type="{scotch_type}", volume_value={scotch_volume}')
        self.execute(
            f'INSERT INTO prices(product_number, scrape_date, price) VALUES ({scotch_number}, "{formatted_date}", \
             "{scotch_price}") ON DUPLICATE KEY UPDATE price="{scotch_price}"')

# TODO: Database backup at each update
#     - Google drive? Remote Server? Email? (db size with a few price updates is around 1mb)

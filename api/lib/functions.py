import os
import yaml
import simplejson as json
from datetime import date

def serialize(obj):
    if isinstance(obj, date):
        serial = obj.isoformat()
        return serial
    return json.dumps(obj, default=lambda x: x.__dict__)

def load_settings():
    """
    Load the config file for variables and credentials.
    :return: a settings object

            example usage of object:
            host = settings['db']['host']
            address = settings['site']['url']
    """
    script_dir = os.path.dirname(__file__)
    file_path = os.path.join(script_dir, '../config.yaml')

    with open(file_path, 'r') as stream:
        config = yaml.safe_load(stream)

    return config


# Load settings. We'll need some of these.
settings = load_settings()

# With settings loaded, instantiate a database connection.
# DBHelper(settings['db']['host'], settings['db']['user'], settings['db']['pass'], settings['db']['db'])

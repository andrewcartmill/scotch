from datetime import date
from flask import Flask
from flask_restful import Resource, Api
from flask_cors import CORS
from sqlalchemy import create_engine
from lib.functions import load_settings, serialize

config = load_settings()

# db_connect = create_engine(f"mysql+pymysql://{config['db']['user']}:{config['db']['pass']}@0.0.0.0:3306"
db_connect = create_engine(f"mysql+pymysql://{config['db']['user']}:{config['db']['pass']}@{config['db']['host']}"
                           f"/{config['db']['db']}")
app = Flask(__name__)
CORS(app)
api = Api(app)


class Price(Resource):
    def __init__(self):
        self.uid = None
        self.prices = []


class Scotch(Resource):

    def __init__(self):
        self.uid = None
        self.name = None
        self.url = None
        self.image_url = None
        self.type = None
        self.volume = None
        self.price = Price()

    def get(self, scotch_id=None):
        conn = db_connect.connect()
        query = conn.execute(
            f"select * from scotch where (product_number = {scotch_id})")
        row_headers = [x[0] for x in query.cursor.description]
        row_values = query.cursor.fetchall()
        json_data = []
        for scotch_result in row_values:
            json_data.append(dict(zip(row_headers, scotch_result)))

            self.uid = json_data[0]['product_number']
            self.name = json_data[0]['scotch_name']
            self.url = json_data[0]['product_url']
            self.image_url = json_data[0]['image_url']
            self.type = json_data[0]['scotch_type']
            self.volume = json_data[0]['volume_value']
            self.price = Price()

            json_price_data = []
            query = conn.execute(
                f"select scrape_date, price from prices where (product_number = {self.uid})")
            row_headers = [x[0] for x in query.cursor.description]
            row_values = query.cursor.fetchall()
            for price_result in row_values:
                scrape_date = price_result[0].strftime('%m/%d/%Y')
                updated_result = (scrape_date, price_result[1])
                json_price_data.append(dict(zip(row_headers, updated_result)))

            self.price = json_price_data

            resp = serialize(self)

            response = app.response_class(
                response=resp,
                status=200,
                mimetype='application/json'
            )
            return response


class Scotches(Resource):
    def get(self):
        scotch_list = []
        conn = db_connect.connect()
        query = conn.execute(f"select * from scotch")
        row_headers = [x[0] for x in query.cursor.description]
        row_values = query.cursor.fetchall()
        json_data = []
        for result in row_values:

            json_data.append(dict(zip(row_headers, result)))
        for s in json_data:
            scotch = Scotch()
            scotch.uid = s['product_number']
            scotch.name = s['scotch_name']
            scotch.url = s['product_url']
            scotch.image_url = s['image_url']
            scotch.type = s['scotch_type']
            scotch.volume = s['volume_value']
            scotch.volume_unit = "ml"
            scotch.price = Price()

            json_price_data = []
            query = conn.execute(
                f"select scrape_date, price from prices where (product_number = {scotch.uid})")
            row_headers = [x[0] for x in query.cursor.description]
            row_values = query.cursor.fetchall()
            for result in row_values:
                scrape_date = result[0].strftime('%m/%d/%Y')
                updated_result = (scrape_date, result[1])
                json_price_data.append(dict(zip(row_headers, updated_result)))

            scotch.price = json_price_data

            scotch_list.append(scotch.__dict__)

        response = app.response_class(
            response=serialize(scotch_list),
            status=200,
            mimetype='application/json'
        )
        return response


api.add_resource(Scotch, '/scotch/<int:scotch_id>')
api.add_resource(Scotches, '/scotch')


if __name__ == '__main__':
    app.run(port='5002', host='0.0.0.0')

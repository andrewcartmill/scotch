from lib.functions import load_settings, get_html, parse_html
from lib.db import DBHelper
import time

settings = load_settings()
url = settings['site_url']
html_file = settings['html_output']
db = DBHelper()

# Collect Data
print("Scraping...")
start = time.time()
get_html(url, html_file)
end = time.time()
print(f"Scraping took {end - start}")

print("Parsing data and updating the database...")
start = time.time()
scotches = parse_html(html_file)
end = time.time()
print(f"HTML Parsing took {end - start}")

start = time.time()
db.write_list_to_db(scotches)
end = time.time()
print(f"Database calls took {end - start}")

print(f"Done!")

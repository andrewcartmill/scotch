import pymysql
from lib.functions import load_settings


class DBHelper:

    def __init__(self):
        settings = load_settings()

        self.host = settings['db']['host']
        self.user = settings['db']['user']
        self.password = settings['db']['pass']
        self.db = settings['db']['db']

    def __connect__(self):
        self.con = pymysql.connect(host=self.host, user=self.user, password=self.password, db=self.db,
                                   cursorclass=pymysql.cursors.DictCursor, autocommit=True)
        self.cur = self.con.cursor()

    def __disconnect__(self):
        self.con.close()

    def fetch(self, sql):
        self.__connect__()
        self.cur.execute(sql)
        result = self.cur.fetchall()
        self.__disconnect__()
        return result

    def execute(self, sql, values):
        self.__connect__()
        self.cur.executemany(sql, values)
        self.__disconnect__()

    def write_list_to_db(self, l):
        """
        Performs chunked writes to our database to add entries for our scotches.

        :param list: A list of lists containing scotches
        :return: None. Values are written to the database
        """

        for i in range(0, len(l), 10):
            sub_list = l[i:i + 10]
            scotch_values = []
            scotch_price_values = []
            for a in sub_list:
                scotch_values.append((a[0], a[1], a[2], a[3], a[4], a[5]))
                scotch_price_values.append((a[0], a[6], a[7]))
            scotch_insert_query = "INSERT INTO scotch(product_number, scotch_name, product_url, image_url, " \
                                  "scotch_type, volume_value) VALUES (%s, %s, %s, %s, %s, %s) " \
                                  "ON DUPLICATE KEY UPDATE scotch_name=VALUES(scotch_name), " \
                                  "product_url=VALUES(product_url), image_url=VALUES(image_url), " \
                                  "scotch_type=VALUES(scotch_type), volume_value=VALUES(volume_value) \n"
            self.execute(scotch_insert_query, scotch_values)

            scotch_price_insert_query = "INSERT INTO prices(product_number, scrape_date, price) " \
                                        "VALUES (%s, %s, %s) " \
                                        "ON DUPLICATE KEY UPDATE price=VALUES(price) \n"
            self.execute(scotch_price_insert_query, scotch_price_values)

# TODO: Database backup at each update
#     - Google drive? Remote Server? Email? (db size with a few price updates is around 1mb)

import os
import re
import yaml
import requests
from requests.adapters import HTTPAdapter
from datetime import datetime
import pint
from bs4 import BeautifulSoup


def load_settings():
    """
    Load the config file for variables and credentials.
    :return: a settings object

            example usage of object:
            host = settings['db']['host']
            address = settings['site']['url']
    """
    script_dir = os.path.dirname(__file__)
    file_path = os.path.join(script_dir, '../config.yaml')

    with open(file_path, 'r') as stream:
        config = yaml.safe_load(stream)

    return config


# Load settings. We'll need some of these.
settings = load_settings()


def get_html(url, html_file):
    """
    Using Beautiful Soup, grab our html and write it to a file. If the file already exists, skip the HTTP request and do
    nothing.

    :param url: The url to fetch with Beautiful Soup
    :param html_file: The output file path
    """
    if os.path.isfile(html_file):
        BeautifulSoup(open(html_file), "html.parser")
        print("File exists. Skipping HTTP request.")
    else:
        print("Getting html.")
        user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, ' \
                     'like Gecko) Chrome/73.0.3683.103 Safari/537.36'
        headers = {'User-Agent': user_agent}
        sess = requests.Session()
        adapter = requests.adapters.HTTPAdapter(max_retries=10)
        sess.mount('http://', adapter)

        page = sess.get(url, headers=headers)
        soup = BeautifulSoup(page.text, "html.parser")

        print("Writing html to file.")
        with open(html_file, "w") as file:
            file.write(str(soup))


def parse_html(file):
    """
    Parse our scotch data from the html and write them to our database.

    :param file: The file to parse.
    :return: a list of lists containing individual scotch details.
    """
    ureg = pint.UnitRegistry()
    now = datetime.now()
    obj_list = []

    if os.path.isfile(file):
        soup = BeautifulSoup(open(file), "html.parser")
        print("Parsing results.")
    else:
        raise FileNotFoundError(f'HTML file not found.')

    for result in soup.find_all("div", "resultats_product"):
        scotch_number_result = (result.find("p", "desc"))
        scotch_number = scotch_number_result.text.strip()[-8:].strip()

        scotch_name_result = result.find("p", "nom").find("a")
        scotch_name = scotch_name_result.get("title").strip()[26:]

        scotch_url_result = result.find("div", "img").find("a")
        scotch_url = scotch_url_result.get('href')

        scotch_image_result = result.find("div", "img").find("img")
        scotch_image = "https:" + scotch_image_result.get('src')

        scotch_price_result = result.find("td", "price")
        scotch_price_find = re.search(r'\$.*$', scotch_price_result.get_text())
        scotch_price = float(scotch_price_find.group(0).replace(",", "").replace("$", ""))

        scotch_volume_result = result.find("p", "desc")
        scotch_volume_content = scotch_volume_result.text.split(',', 1)[-1]
        scotch_volume_stripped = scotch_volume_content.lstrip().split('\n', 1)[0]

        # TODO: Implement multi bottle products better instead of just adding volumes together. Maybe a
        #  "contains_multiple" column?

        if "X" in scotch_volume_stripped  and "ml" in scotch_volume_stripped:
            vol_split = scotch_volume_stripped.split()
            scotch_volume = int(vol_split[0]) * int(vol_split[2])
        elif "X" in scotch_volume_stripped and "L" in scotch_volume_stripped:
            vol_split = scotch_volume_stripped.split()
            scotch_volume = (int(vol_split[0]) * int(vol_split[2])) * 1000
        else:
            scotch_volume = ureg(scotch_volume_stripped).to('millilitre').magnitude

        scotch_type_result = result.find("p", "desc")
        scotch_type_desc_list = scotch_type_result.text.strip().split('\n', 1)[: 1]
        scotch_type = str(scotch_type_desc_list[0])

        formatted_date = now.strftime('%Y-%m-%d')

        scotch_obj = [scotch_number, scotch_name, scotch_url, scotch_image, scotch_type, scotch_volume,
                      formatted_date, scotch_price]

        obj_list.append(scotch_obj)

    return obj_list

        # TODO: HTML file is removed after successful write in production

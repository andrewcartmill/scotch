CREATE TABLE `scotch` (
  `product_number` int(20) NOT NULL,
  `scotch_name` varchar(100) NOT NULL,
  `product_url` varchar(200) NOT NULL,
  `image_url` varchar(200) DEFAULT NULL,
  `scotch_type` varchar(100) NOT NULL,
  `volume_value` decimal(16,2) DEFAULT NULL,
  PRIMARY KEY (`product_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `prices` (
  `product_number` int(20) DEFAULT NULL,
  `scrape_date` date DEFAULT NULL,
  `price` decimal(16,2) DEFAULT NULL,
  UNIQUE KEY `product_number` (`product_number`,`scrape_date`),
  CONSTRAINT `fk_1` FOREIGN KEY (`product_number`) REFERENCES `scotch` (`product_number`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

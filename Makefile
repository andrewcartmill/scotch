# --------------------------------------------------------------------
# Copyright (c) 2019 LINKIT, The Netherlands. All Rights Reserved.
# Author(s): Anthony Potappel
# 
# This software may be modified and distributed under the terms of the
# MIT license. See the LICENSE file for details.
# --------------------------------------------------------------------

# PROJECT_NAME defaults to name of the current directory.
# should not to be changed if you follow GitOps operating procedures.
PROJECT_NAME = $(notdir $(PWD))

# Note. If you change this, you also need to update docker-compose.yml.
# only useful in a setting with multiple services/ makefiles.
SERVICE_TARGET := main

THIS_FILE := $(lastword $(MAKEFILE_LIST))
CMD_ARGUMENTS ?= $(cmd)

# export such that its passed to shell functions for Docker to pick up.
export PROJECT_NAME
export HOST_USER
export HOST_UID

# suppress makes own output
#.SILENT:

# Regular Makefile part for buildpypi itself
help:
	@echo ''
	@echo 'Usage: make [TARGET] [EXTRA_ARGUMENTS]'
	@echo 'Targets:'
	@echo '  build    	build docker --image-- '
	@echo '  rebuild  	rebuild docker --image-- '
	@echo '  prune    	shortcut for docker system prune -af. Cleanup inactive containers and cache.'
	@echo ''

up:
	docker-compose up

rebuild:
	# force a rebuild by passing --no-cache
	docker container prune && docker image prune && cd api && docker build . && cd .. && cd harvester && docker build . && cd .. && cd storage && docker build . && docker-compose up --build

build:
	# only build the container. Note, docker does this also if you apply other targets.
	docker build .

prune:
	# clean all that is not actively used
	docker system prune -af
